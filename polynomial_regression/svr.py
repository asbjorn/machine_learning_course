import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

dataset = pd.read_csv('Position_Salaries.csv')
Xd = dataset.iloc[:, 1:2].values
yd = dataset.iloc[:, 2].values

# Feature scaling
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
sc_y = StandardScaler()
X = sc_X.fit_transform(Xd)
y = np.ravel(sc_y.fit_transform(yd.reshape(-1,1)))

# Creating svr model
from sklearn.svm import SVR
regressor = SVR(kernel='rbf')

# Fitting svr model to data
regressor.fit(X,y)

y_pred = sc_y.inverse_transform(regressor.predict(sc_X.transform(6.5)))


plt.scatter(X,y,color='red')
plt.plot(X,regressor.predict(X), color='blue')
plt.title('Truth or bluff (SVR)')
plt.xlabel('Positin level')
plt.ylabel('Salary')
plt.show()




